const express = require("express");
const path = require("path");
const app = express();
const server = require("http").Server(app);
const io = require("socket.io")(server);
const fs = require("fs");
const port = 8080;

app.use("/", express.static(path.join(__dirname, "../client/dist/chatapp")));
app.use("/audio", express.static(path.join(__dirname, "converted")));

io.on("connection", socket => {
    console.log("new connection made from client with ID=" + socket.id);
    socket.on("newMsg", data => {
        io.sockets.emit("msg", {msg: data.msg, timeStamp: getCurrentDate(), author: data.author});
    });

    socket.on("convert", data => {
        convert(data.msg, socket.id);
    });
});

server.listen(port, () => {
    console.log("Listening on port " + port);
});

function getCurrentDate() {
    let d = new Date();
    return d.toLocaleString();
}

// https://cloud.google.com/text-to-speech/docs/reference/libraries#using_the_client_library
function convert(text, id) {
    console.log(text);
    const textToSpeech = require("@google-cloud/text-to-speech");
    const client = new textToSpeech.TextToSpeechClient({
        projectId: 'fit2095-256520',
        keyFilename: '/home/dev/FIT2095-0fdb36e0c410.json',
    });
    const request = {
        input: {text: text.msg},
        // Select the language and SSML Voice Gender (optional)
        voice: {languageCode: "en-US", ssmlGender: "NEUTRAL"},
        // Select the type of audio encoding
        audioConfig: {audioEncoding: "MP3"},
    };
    // Performs the Text-to-Speech request
    return client.synthesizeSpeech(request, (err, response) => {
        if (err) {
            console.error("ERROR:", err);
            return false;
        }
        // Write the binary audio content to a local file
        fs.writeFile("converted/" + id + ".mp3", response.audioContent, "binary", err => {
            if (err) {
                console.error("ERROR:", err);
                return false;
            }
            console.log("Audio content written to file: " + id + ".mp3");
            io.to(`${id}`).emit("convert", {status: true, id: id});
        });
        return false;
    });
}
